from django.db import models
from django.utils.translation import gettext_lazy as _


class Device(models.Model):
    device_id = models.CharField(max_length=64, unique=True)
    token = models.CharField(max_length=64)

    def __str__(self):
        return self.device_id


class Payload(models.Model):
    device = models.ForeignKey(
        "Device", verbose_name=_("Device"), on_delete=models.CASCADE
    )
    supply_voltage = models.IntegerField()
    tx_reason = models.CharField(max_length=200)
    hdop = models.FloatField()
    altitude = models.FloatField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    received_at = models.DateTimeField()

    def __str__(self):
        return f"{self.device_id} - {self.tx_reason} - {self.received_at}"
