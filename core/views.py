import json
import logging
from pprint import pprint

from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from .models import Device, Payload

logger = logging.getLogger(__name__)


# Create your views here.
@csrf_exempt
def webhooks(request, device_id):
    logger.info("webhooks")
    if request.method == "POST":
        token = request.META.get("HTTP_AUTHORIZATION").split(" ")[1]
        device = get_object_or_404(Device, device_id=device_id, token=token)
        json_data = json.loads(request.body)
        logger.debug(json_data)
        payload = json_data["uplink_message"]["decoded_payload"]
        received_at = json_data["received_at"]
        try:
            Payload.objects.create(
                device=device,
                supply_voltage=payload["Supply_Voltage"],
                tx_reason=payload["TX_Reason"],
                hdop=payload["hdop"],
                altitude=payload["altitude"],
                latitude=payload["latitude"],
                longitude=payload["longitude"],
                received_at=received_at,
            )
        except Exception as e:
            logger.error(e)
            logger.debug(payload)
            return HttpResponse(status=400)
        return JsonResponse({"device": device_id, "received_at": received_at})
    else:
        return HttpResponse(status=405)


@login_required
def location_list(request):
    logger.info("location_list")
    payloads = Payload.objects.all().order_by("-received_at")[:10]
    return render(request, "core/location_list.html", {"payloads": payloads})
