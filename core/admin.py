# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Device, Payload


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ("id", "device_id", "token")


@admin.register(Payload)
class PayloadAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "device",
        "supply_voltage",
        "tx_reason",
        "hdop",
        "altitude",
        "latitude",
        "longitude",
        "received_at",
    )
    list_filter = ("device_id", "received_at")
