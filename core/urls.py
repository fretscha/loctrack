from . import views
from django.urls import path, include
from django.views.generic import RedirectView

urlpatterns = [
    path("", RedirectView.as_view(pattern_name='location_list', permanent=False)),
    path("webhooks/<str:device_id>/", views.webhooks, name="webhooks"),
    path("location/", views.location_list, name="location_list"),
]
